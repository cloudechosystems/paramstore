#What does it do?

Paramstore API provides PUT and GET methods to create/update a json payload and fetch it.

Requirement_Note.pdf and Technical_Note.pdf will give details of what paramstore does and how it is implemented.

https://drive.google.com/file/d/1F-IlKZAohInqXeMN5BQMyC2Zux34VoHs/view?usp=sharing <br/>

https://drive.google.com/file/d/1919jpOnohAaAfpzz1gQL09pULQ-VDPP6/view?usp=sharing

Please note that this was created based on someone asking the question can we have sample api to create/update a json payload and fetch it. 
No thought has been put as what they are trying to achieve / are there other options like say https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html
which would cater to the need.

#How to deploy?
You would need aws cli with access and credentials setup towards the aws environment where you want to deploy this API.

aws cloudformation deploy --template-file cf.yaml --stack-name {Your-Preferred-StackName} --capabilities CAPABILITY_IAM --parameter-overrides S3BucketName={Your-Preferred-S3BucketName}

Example:

aws cloudformation deploy --template-file cf.yaml --stack-name Dev --capabilities CAPABILITY_IAM --parameter-overrides S3BucketName=DevS3Bucket